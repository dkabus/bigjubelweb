#!/bin/env python3
"Launch BigJubel Web from the command line"
import os
import subprocess
import sys
os.environ["FLASK_APP"] = "bigjubelweb"

def main():
    subprocess.run(["flask", *sys.argv[1:]])
