from flask import Blueprint, current_app, flash, g, redirect, render_template, request, session, url_for
from functools import wraps
from re import compile
from werkzeug.security import check_password_hash, generate_password_hash
from .database import get as get_db
from .language import lang

EMAIL_REGEX = compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
USERNAME_REGEX = compile(r"(^[a-zA-Z0-9_.+-]+$)")

bp = Blueprint("auth", __name__, url_prefix="/auth")

def get_user(username=None, user_id=None):
    """
        get user
        1. by username
        2. by user_id
        3. current user
        4. None
    """
    db = get_db()
    if username is not None:
        return db.execute(
            "SELECT * FROM user WHERE lower(username) = ?", (username.lower(),)
        ).fetchone()
    elif user_id is not None:
        return db.execute(
            "SELECT * FROM user WHERE id = ?", (user_id,)
        ).fetchone()
    elif current_app.config["USER_MODE"] <= 1: # single user mode
        return get_user(username=current_app.config["ADMIN_USERNAME"])
    elif session.get("user_id"):
        return get_user(user_id=session.get("user_id"))
    else:
        return None

@bp.route("/register", methods=["POST"])
def register():
    """
        register a user with the fields
        - username, checked for duplicates
        - email, checked if valid
        - password, at least 8 characters long
        - password repeat
    """
    if current_app.config["USER_MODE"] <= 1: # single user mode
        return redirect(url_for("main"))

    privacy = request.form.get("privacy", False)
    username = request.form["username"]
    email = request.form["email"]
    password = request.form["password"]
    repeat = request.form["repeat"]

    if not privacy:
        flash(lang("required field") + ": " + lang("accept privacy"))
    elif not username:
        flash(lang("required field") + ": " + lang("username"))
    elif not email:
        flash(lang("required field") + ": " + lang("email"))
    elif not password:
        flash(lang("required field") + ": " + lang("password"))
    elif len(password) < 8:
        flash(lang("invalid input") + ": " + lang("password") + " " + lang("too short"))
    elif password != repeat:
        flash(lang("invalid input") + ": " + lang("repeat password"))
    elif not USERNAME_REGEX.match(username):
        flash(lang("invalid input") + ": " + lang("username"))
    elif not EMAIL_REGEX.match(email):
        flash(lang("invalid input") + ": " + lang("email"))
    elif get_user(username) is not None:
        flash(lang("username exists"))
    else:
        insert_user(username, password, email)
        flash(lang("register") + " " + lang("successful"))

    return redirect(url_for("main"))

def insert_user(username, password, email=None):
    db = get_db()
    db.execute(
        "INSERT INTO user (username, email, password) VALUES (?, ?, ?)",
        (username, email or "", generate_password_hash(password))
    )
    db.commit()
    print("SUCCESS: inserted user '{}' <{}> to database".format(username, email or ""))

@bp.route("/login", methods=["POST"])
def login():
    """
        check email and password combination and log in the user
    """
    if current_app.config["USER_MODE"] <= 1: # single user mode
        return redirect(url_for("main"))

    username = request.form["username"]
    password = request.form["password"]
    user = get_user(username)

    if user is None:
        flash(lang("invalid input") + ": " + lang("username"))
    elif not check_password_hash(user["password"], password):
        flash(lang("invalid input") + ": " + lang("password"))
    else:
        session.clear()
        session.permanent = True # keep session for 31 days
        session["user_id"] = user["id"]
        flash(lang("login") + " " + lang("successful") + ". " + lang("logged in as") + " " + user["username"])

    return redirect(url_for("main"))

@bp.route("/logout")
def logout():
    """
        clears the session
    """
    session.clear()
    flash(lang("logout") + " " + lang("successful"))
    return redirect(url_for("main"))

@bp.before_app_request
def load_logged_in_user():
    """
        make sure, the user is logged in
    """
    g.user = get_user()

def login_required(view):
    """
        implements @login_required function decorator,
        which makes a function only accessible when logged in
    """
    @wraps(view)
    def wrapped_view(**kwargs):
        if current_app.config["USER_MODE"] == 0:
            return redirect(url_for("main"))

        if g.user is None:
            flash(lang("must login"))
            return redirect(url_for("main"))

        return view(**kwargs)

    return wrapped_view
