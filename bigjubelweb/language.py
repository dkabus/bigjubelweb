import subprocess
from flask import g
from datetime import date
from os import environ

web_version = "0.7.7"
cli_version = subprocess.Popen(
        ["bigjubel", "-v"],
        stdout=subprocess.PIPE,
).communicate()[0].decode().replace("\r","").replace("\n","")
desktop_version = environ.get("BIGJUBEL_DESKTOP_VERSION")

authors = {
    "DAKsoft": 1999,
    "Diethelm Kabus": 1999,
    "Desmond Kabus": 2019,
}
y1 = date.today().year
copyright = "\n".join("© {} - {} {}".format(y0, y1, a) for a, y0 in authors.items())

license_notice = """
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
"""

common = {
    "bigjubel": "BigJubel",
    "copyright": copyright,
    "version cli": "BigJubel CLI " + cli_version,
    "version web": "BigJubel Web " + web_version,
    "license notice": license_notice,
}
if desktop_version:
    common["version desktop"] = "BigJubel Desktop " + desktop_version

de_event_example = """Beispiel:
06.03.1999.Die erste funktionierende Version von BigJubel ist nun schon {3} alt

Platzhalter für grammatikalische Fälle:
"{0}" Nominativ "3 Jahre sind vergangen"
"{1}" Genitiv   "1 Tages"
"{2}" Dativ     "seit 4 Monaten"
"{3}" Akkusativ "schon 1 Woche alt"
"#"   alternativer Platzhalter für den Akkusativ"""

de = {**common,
    "aborted": "abgebrochen",
    "about": "Infos und Rechtliches",
    "accept privacy": "Ich habe die Datenschutzerklärung gelesen und akzeptiere sie",
    "accept": "Absenden",
    "anniversary today": "Heute ist ein Jahrestag",
    "append to events": "An bisherige Ereignisliste anhängen",
    "autoplay": "Automatisch Ton abspielen",
    "change events": "Ereignisse verändern",
    "changes accepted": "Änderungen übernommen",
    "date": "Datum",
    "delete all": "Alle löschen",
    "delete": "Löschen",
    "email": "E-Mail-Adresse",
    "event example": de_event_example,
    "example": "Beispiel",
    "export and import": "Ereignisliste exportieren und importieren",
    "export description": "Hier kann die Ereignisliste als Textdatei heruntergeladen werden. Beim Importieren wird der Inhalt der hochgeladenen Ereignisliste an die bestehende Liste angehangen.",
    "export events": "Ereignisliste exportieren",
    "import events": "Ereignisliste importieren",
    "imprint": "Impressum",
    "invalid input": "Ungültige Eingabe",
    "lang": "de",
    "language": "Sprache",
    "license": "Lizenz",
    "logged in as": "Angemeldet als",
    "login": "Anmelden",
    "logout": "Abmelden",
    "message": "Meldung",
    "must login": "Hierfür ist eine Anmeldung erforderlich",
    "no admin user": "In der Datenbank ist der Admin-Benutzer noch nicht angelegt",
    "no events given": "Keine Ereignisse angegeben",
    "no messages": "Keine Meldungen im gewählten Zeitraum",
    "no": "nein",
    "password": "Passwort",
    "privacy policy": "Datenschutzerklärung",
    "project homepage": "Mehr über BigJubel",
    "register": "Registrieren",
    "repeat password": "Passwort wiederholen",
    "required field": "Pflichtfeld",
    "settings": "Einstellungen",
    "sort date rev": "Nach Datum (rückwärts)",
    "sort date": "Nach Datum",
    "sort no": "Ursprüngliche Reihenfolge",
    "sort": "Sortieren",
    "startday": "Anzahl Tage vor heute",
    "stopday": "Anzahl Tage nach heute",
    "successful": "erfolgreich",
    "toggle sound": "Ton an/aus",
    "too short": "zu kurz",
    "username exists": "Dieser Benutzername wird schon benutzt",
    "username": "Benutzername",
    "yes": "ja",
}

en = {**de,
    "aborted": "aborted",
    "about": "about and legal",
    "accept privacy": "I have read and accept the privacy policy",
    "accept": "submit",
    "anniversary today": "There is an anniversary today",
    "append to events": "append to previous events list",
    "autoplay": "autoplay sound",
    "change events": "modify events",
    "changes accepted": "changes accepted",
    "date": "date",
    "delete all": "delete all",
    "delete": "delete",
    "email": "email",
    "event example": "06.03.1999.Today, the first working version of BigJubel is # old",
    "example": "example",
    "export and import": "export and import events",
    "export description": "The list of events can here be downloaded as a plain text file. When uploading an events file, it will be appended to the existing events list.",
    "export events": "export events",
    "import events": "import events",
    "imprint": "legal information",
    "invalid input": "invalid input",
    "lang": "en",
    "language": "language",
    "license": "license",
    "logged in as": "logged in as",
    "login": "log in",
    "logout": "log out",
    "message": "message",
    "must login": "you must be logged in for this",
    "no admin user": "the admin user does not yet exist in the database",
    "no events given": "no events given",
    "no messages": "no messages in the selected timespan",
    "no": "no",
    "password": "password",
    "privacy policy": "privacy policy",
    "project homepage": "about BigJubel",
    "register": "register",
    "repeat password": "repeat password",
    "required field": "required field",
    "settings": "settings",
    "sort date rev": "sort by date (reversed)",
    "sort date": "sort by date",
    "sort no": "restore original order",
    "sort": "sort",
    "startday": "number of days before today",
    "stopday": "number of days after today",
    "successful": "successful",
    "toggle sound": "turn sound on/off",
    "too short": "too short",
    "username exists": "this username is already taken",
    "username": "username",
    "yes": "yes",
}

strings = dict(de=de, en=en)

class Language_Code:
    """
        a small wrapper for the available language codes
    """
    def __init__(self, lang):
        if not lang in strings.keys():
            raise ValueError("Available languages: " + ", ".join(strings.keys()))
        self.lang = lang
    def __str__(self):
        return self.lang
    def to_base_type(self):
        return self.lang

class Bool:
    """
        a small wrapper to configure a bool using strings
    """
    def __init__(self, b):
        if type(b) == type(True):
            self.b = b
        elif b.lower() in ["1", "true", "yes", "ja"]:
            self.b = True
        elif b.lower() in ["0", "false", "no", "nein"]:
            self.b = False
        else:
            raise ValueError("Bool must be True or False")
    def __str__(self):
        return self.b and lang("yes") or lang("no")
    def to_base_type(self):
        return self.b

def lang(key, language_code=None, placeholder=True, warn=True):
    """
        get the string for given key and language_code
    """
    if language_code is None:
        from .storage import get_settings
        language_code = get_settings()["language"]
    s = strings[str(language_code)].get(key)
    if not s:
        if warn:
            print("'{}' not found in dictionary '{}'".format(key, language_code))
        s = placeholder and "[" + key + "]" or None
    return s
