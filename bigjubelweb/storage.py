from flask import Blueprint, current_app, flash, g, redirect, render_template, request, session, url_for
from .database import get as get_db
from . import authentication as auth
import yaml
from .language import lang, Bool, Language_Code

bp = Blueprint("storage", __name__, url_prefix="/storage")

@bp.route("/events", methods=["POST"])
@auth.login_required
def append_events(events=None):
    """
        store events in database
    """
    if events is None:
        events = request.form["value"].replace("\r", "").split("\n")

    db = get_db()
    for e in events:
        if len(e) > 0:
            db.execute("INSERT INTO event (ownerid, text) VALUES (?, ?)", (g.user["id"], e))
    db.commit()

    flash(lang("changes accepted"))
    return redirect(url_for("main"))

@bp.route("/delete-all-events/")
@auth.login_required
def delete_events():
    """
        delete all events of current user
    """
    db = get_db()
    db.execute("DELETE FROM event WHERE ownerid = ?", (g.user["id"],))
    db.commit()

    flash(" ".join((lang("delete all"), lang("successful"))))
    return redirect(url_for("main"))

@bp.route("/delete-event/<eid>")
@auth.login_required
def delete_event(eid):
    """
        delete a single event from database
    """
    text = (get_event(eid) or dict(text=""))["text"]

    db = get_db()
    db.execute("DELETE FROM event WHERE ownerid = ? AND id = ?", (g.user["id"], eid,))
    db.commit()

    flash("{} {}: '{}'".format(lang("delete"), lang("successful"), text))
    return redirect(url_for("main"))

@bp.route("/bigjubel-events.txt")
@auth.login_required
def export_events():
    """
        return text file with events
    """
    content = "\n".join(e["text"] for e in get_events()) + "\n"
    if lang("version desktop", placeholder=False): # if BigJubel desktop
        import webview
        import os
        if len(webview.windows) > 0:
            w = webview.windows[0]
            filename = "".join(
                w.create_file_dialog(
                    webview.SAVE_DIALOG,
                    save_filename='bigjubel-events.txt'
                )
            )
            with open(filename, "w") as f:
                f.write(content)
            flash("{} {}: {}".format(lang("export events"), lang("successful"), filename))
            w.load_url(w.get_current_url())
    return content

@bp.route("/import-events", methods=['POST'])
@auth.login_required
def import_events():
    """
        upload a text file
    """
    return append_events(events=request.files['file'].read().decode().replace("\r", "").split("\n"))

@bp.route("/sort-events/<key>/<rev>")
@auth.login_required
def sort_events(key, rev):
    """
        sort events according to key,
        rev=1 for reversed order
    """
    rev = int(rev)
    db = get_db()
    events = get_events()
    events = dict(zip((e["id"] for e in events), (e["text"] for e in events)))
    for eid, e in events.items():
        sortkey = 0
        if key == "date":
            day, month, year = e[0:2], e[3:5], e[6:10]
            try:
                sortkey = int("".join([year, month, day]))
            except ValueError:
                continue
        assert(type(sortkey) == type(1))
        db.execute(
            "UPDATE event SET sortkey = ? WHERE id = ?",
            (rev and -sortkey or sortkey, eid)
        )
    db.commit()

    flash(" ".join([lang("sort"), lang("successful")]))
    return redirect(url_for("main"))

def convert(value, type_, default):
    """
        try to convert value to type.
        if not possible, flash error and return default.
    """
    if value is None:
        return default
    try:
        return type_(value)
    except Exception as e:
        flash(lang("invalid input") + " " + type_.__name__ + ": " + str(value))
        flash(str(e))
        return default

def default_settings():
    """
        construct default settings dictionary.
    """
    return dict(
            autoplay=Bool(current_app.config["AUTOPLAY"]),
            language=Language_Code(current_app.config["LANGUAGE"]),
            startday=current_app.config["STARTDAY"],
            stopday=current_app.config["STOPDAY"],
    )

@bp.route("/settings", methods=["POST"])
@auth.login_required
def set_settings():
    """
        store events in database for the logged in user
    """
    settings = {}
    default = default_settings()
    for label in default.keys():
        d = default[label]
        settings[label] = convert(request.form[label], type(d), d)
        if hasattr(settings[label], "to_base_type"):
            settings[label] = settings[label].to_base_type()

    settings = yaml.dump(settings)

    db = get_db()
    db.execute(
        "UPDATE user SET settings = ? WHERE id = ?",
        (settings, g.user["id"])
    )
    db.commit()

    flash(lang("changes accepted"))
    return redirect(url_for("main"))

def get(key):
    """
        get the current users or admins field with the given key
    """
    if g.user:
        return g.user[key]

    admin = auth.get_user(current_app.config["ADMIN_USERNAME"])
    if admin:
        return admin[key]

    return None

def get_event(eid):
    """
        read events from database by id for current user
    """
    u = g.user or auth.get_user(current_app.config["ADMIN_USERNAME"])
    if not u:
        return None

    return g.db.execute(
        "SELECT * FROM event WHERE ownerid = ? AND id = ?", (u["id"], eid,)
    ).fetchone()

def get_events():
    """
        read events from database for current user
    """
    u = g.user or auth.get_user(current_app.config["ADMIN_USERNAME"])
    if not u:
        return None

    return sorted(g.db.execute(
        "SELECT * FROM event WHERE ownerid = ?", (u["id"],)
    ).fetchall(), key=lambda e: int(e["sortkey"]))


def get_settings():
    """
        read current settings from database for current user
    """
    settings = yaml.safe_load(get("settings") or "{}")
    default = default_settings()
    for label in default.keys():
        settings[label] = settings.get(label, default[label])
    return settings
