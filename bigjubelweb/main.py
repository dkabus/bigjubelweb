#!/bin/env python3
import os
import subprocess
import yaml
from flask import Flask, current_app, render_template, flash, g, redirect, url_for
from flask import __version__ as flask_version
from . import database as db
from . import authentication as auth
from . import storage
from .language import lang

def bigjubel(eventstring, startday=None, stopday=None, language_code=None):
    """
        run the command line client of `bigjubel`
    """
    if os.path.isfile("bigjubel"):
        cmd = "./bigjubel"
    else:
        cmd = "bigjubel"
    cmd = [cmd, "-y", "-m100"]

    if startday is not None:
        cmd.append("-b" + str(startday))
    if stopday is not None:
        cmd.append("-e" + str(stopday))
    if language_code is not None:
        cmd.append("-l" + str(language_code))

    process = subprocess.Popen(cmd,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    output, errors = process.communicate(input=eventstring.encode())
    output = output.decode()
    output = yaml.safe_load(output or "") or {}

    errors = errors or b""
    errors = errors.decode()
    errors = [l for l in errors.split("\n") if len(l) > 1]
    for error in errors:
        flash(error)

    return output.get("title") or "", output.get("jubels") or []

def main():
    """
        render the website
    """
    events = storage.get_events()
    eventstring = "\n".join(e["text"] for e in events or [])
    jubels = []
    settings = storage.get_settings()
    title = ""

    if not g.user and auth.get_user(current_app.config["ADMIN_USERNAME"]) is None:
        if current_app.config["USER_MODE"] <= 1: # single user mode
            auth.insert_user(
                    username=current_app.config["ADMIN_USERNAME"],
                    password=current_app.config["ADMIN_DEFAULT_PASSWORD"],
            )
            return redirect(url_for("main"))
        flash(lang("no admin user"))
    elif not events:
        flash(lang("no events given"))
    else:
        title, jubels = bigjubel(eventstring,
            settings["startday"], settings["stopday"],
            settings["language"])
        if len(jubels) == 0:
            flash(lang("no messages"))

    anniversary_today = False
    for jubel in jubels:
        if jubel["todayflag"] and jubel["yearflag"]:
            anniversary_today = True

    return render_template("main.html",
            events=events or "",
            jubels=jubels,
            settings=settings,
            title=title,
            lang=lang,
            anniversary_today=anniversary_today,
            user_mode=current_app.config["USER_MODE"],
            instance_name=current_app.config["INSTANCE_NAME"],
    )

def create_app():
    """
        create and configure the app
    """
    xdg = os.environ.get("XDG_DATA_HOME", os.path.join(os.environ["HOME"], ".local", "share"))
    app = Flask(
            __name__,
            instance_path=os.path.join(xdg, "bigjubelweb"),
            instance_relative_config=True
    )

    # load the default config
    app.config.from_mapping(DATABASE=os.path.join(app.instance_path, "bigjubel.sqlite"))
    app.config.from_pyfile(os.path.join(app.root_path, "config.py"))

    # automatically generate instance config
    if app.config["AUTO_INIT_CONFIG"]:
        db.generate_config(path=app.instance_path, root=app.root_path, warn=False)

    # load the instance config, if it exists
    app.config.from_pyfile("config.py", silent=True)

    # warn if using development key
    if app.config["SECRET_KEY"] == "dev":
        print("WARNING: USING DEVELOPMENT KEY")
        print("use `bigjubelweb config-init` to generate a secret key")

    # register modules and blueprints
    db.register(app)
    app.register_blueprint(auth.bp)
    app.register_blueprint(storage.bp)

    # add url rules
    app.add_url_rule("/", "main", main)

    return app

# check for old flask versions, which require the app to be created
if type(flask_version) == type("") and int(flask_version.split(".")[0]) < 1:
    app = create_app()

# run development server
if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)
