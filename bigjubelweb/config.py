#!/bin/env python3

### CONFIGURATION FILE FOR BIGJUBEL WEB ###
#
# Use `bigjubelweb config-init` to create a copy of the default
# file at "~/.local/share/bigjubelweb/config.py"
# with a random SECRET_KEY.
# You may change settings in the created file.
#
# Besides the variables in this file, all other Flask config
# variables can be changed. Confer:
# https://flask.palletsprojects.com/en/1.1.x/config/

### INSTANCE_NAME ###
# The name of this instance of BigJubel.
# Most importantly, this sets the title of the webinterface.
#
# Type: string
INSTANCE_NAME="BigJubel Web"

### SECRET_KEY ###
# A secret key that will be used for securely signing the session
# cookie and can be used for any other security related needs by
# extensions or your application. It should be a long random string
# of bytes, although unicode is accepted too. For example, copy the
# output of this to your config:
# ```
# $ python -c 'import os; print(os.urandom(16))'
# b'_5#y2L"F4Q8z\n\xec]/'
# ```
#
# Do not reveal the secret key when posting questions or
# committing code!
#
# Type: string or bytes-array
SECRET_KEY = "dev"

### DEBUG ###
# Whether debug mode is enabled. When using flask run to start
# the development server, an interactive debugger will be shown
# for unhandled exceptions, and the server will be reloaded when
# code changes. The debug attribute maps to this config key. This
# is enabled when ENV is 'development' and is overridden by the
# FLASK_DEBUG environment variable.
# It may not behave as expected if set in code.
#
# Do not enable debug mode when deploying in production.
#
# Type: True or False.
DEBUG = False

### DATABASE ###
# The absolute path to the SQLite database file.
# If unset, the path is set to "bigjubel.sqlite" within the
# instance path, i.e. by default:
#DATABASE = "/home/myuser/.local/share/bigjubelweb/bigjubel.sqlite"

### USER_MODE ###
# The database can store events and settings for multiple user
# accounts. This is particularly useful when running a BigJubel
# instance as a web-service. However, for local, offline use, one
# might only want to use one user.
#
# USER_MODE can be set to choose the desired behaviour:
#
# 2: multiple user profiles with login screen.
#    The jubilees of the admin (ADMIN_USERNAME) are displayed
#    when not logged in, but the events list can not be modified.
#    Log in with the admin user or any other account to display
#    their jubilees and modify their events and settings, etc.
#
# 1: one user profile. (default)
#    The admin is automatically logged in. For this the variable
#    ADMIN_USERNAME needs to be set. The events list can be modified.
#
# 0: one user profile, not logged in.
#    In this mode, the admins jubilees are displayed but the events
#    can not be modified
USER_MODE = 1

### ADMIN_USERNAME ###
# The username of the special admin account. For instance,
# this account's jubilees are displayed when logged out.
#
# Type: case-insensitive string
ADMIN_USERNAME="admin"

### ADMIN_DEFAULT_PASSWORD ###
# You will probably never need this password.
# It is only required, when changing USER_MODE from 0 or 1 to 2.
#
# When using single user mode and the admin account (ADMIN_USERNAME)
# does not exist, the admin account is created in the database with
# this password.
#
# When using `bigjubelweb config-init`, a random password is created
# for the instance configuration file.
#
# Type: string
ADMIN_DEFAULT_PASSWORD="pleasechangepassword"

### AUTOPLAY ###
# Whether to automatically play a tune on anniversaries.
#
# Type: True or False.
AUTOPLAY = True

### LANGUAGE ###
# The default language to use for this instance. Users can change the
# language in their individual settings.
# This is passed to BigJubel Cli with the option "-l"
#
# Currently supported:
# - de: German (Deutsch)
# - en: English
#
# Type: two-letter language code.
LANGUAGE = "en"

### STARTDAY ###
# How many days before today should BigJubel start calculating
# jubilees? This is passed to BigJubel CLI with the option "-b".
#
# Type: integer.
STARTDAY = 10

### STOPDAY ###
# Analogously to STARTDAY, stop after this number of days after today.
# Passed to BigJubel CLI with "-e"
#
# Type: integer.
STOPDAY = 20

### AUTO_INIT_CONFIG ###
# Automatically create a configuration file in the instance folder
# with a copy of the default settings and a random SECRET_KEY.
# This is useful for a great out-of-the-box-experience:
# Launch the server and everything just sets itself up.
# If you want to host a more sophisticated setup, i.e. with multiple
# or when migrating from other configurations, you might want to turn
# this off.
#
# The BigJubel Web server NEVER overwrites an existing config file.
# If you want to overwrite, you must manually remove or rename the
# old config.py.
#
# You can manually initialise the configuration file with:
# ```
# bigjubelweb config-init
# ```
#
# Type: True or False.
AUTO_INIT_CONFIG = True

### AUTO_INIT_DB ###
# Similar to AUTO_INIT_CONFIG: Automatically create an empty database
# if no database is found in the instance folder.
#
# You can manually initialise the database with:
# ```
# bigjubelweb db-init
# ```
#
# Type: True or False.
AUTO_INIT_DB = True

### AUTO_UPDATE_DB ###
# In future updates, the database format might change.
# When this option is enabled, the database is automatically updated
# to the new format. A backup is kept with the extension ".backup",
# so you can easily revert to the old format if something should go
# wrong.
#
# You can manually update the database with:
# ```
# bigjubelweb db-update
# ```
#
# Type: True or False.
AUTO_UPDATE_DB = True
