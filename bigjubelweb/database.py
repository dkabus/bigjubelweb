import sqlite3
import click
import os
from flask import current_app, g
from flask.cli import with_appcontext
from shutil import copyfile

schema_version = 1

def get(check_version=True, check_exists=True):
    """
        connect the current session g to the database
    """
    filename = current_app.config["DATABASE"]
    if "db" not in g:
        if check_exists and not os.path.exists(filename):
            if current_app.config["AUTO_INIT_DB"]:
                init(filename=filename, warn=False)
            else:
                raise FileNotFoundError("No sqlite database at `{}`! "
                "Initialise with `bigjubelweb db-init`.".format(filename))
        g.db = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)
        g.db.row_factory = sqlite3.Row
    if check_version and g.db.execute("PRAGMA user_version").fetchone()["user_version"] != schema_version:
        if current_app.config["AUTO_UPDATE_DB"]:
            print("Incompatible database version! Automatic update...")
            update()
        else:
            raise Exception("Incompatible database version! Update database with `bigjubelweb db-update`.")
    return g.db

def close(e=None):
    """
        close the current sessions g database connection
    """
    db = g.pop("db", None)
    if db is not None:
        db.close()

def executescript(db, filename):
    print("Executing script", filename)
    with current_app.open_resource(filename) as f:
        db.executescript(f.read().decode("utf8"))

def generate_config(path=None, root=None, warn=True):
    """
        create a new config file with a random secret key
    """
    os.makedirs(path or current_app.instance_path, exist_ok=True)
    cfg = os.path.join(path or current_app.instance_path, "config.py")
    dcfg = os.path.join(root or current_app.root_path, "config.py")

    if os.path.exists(cfg):
        if warn:
            print("WARNING: No config file generated. "
            "Another config file already exists at '{}'.".format(cfg))
        return

    # The next lines are roughly equivalent to:
    # some_modifications < dcfg > cfg
    with open(dcfg, "r") as default:
        with open(cfg, "w") as config:
            for line in default:
                # generate a random key
                if "SECRET_KEY" in line and "\"dev\"" in line:
                    config.write("SECRET_KEY = {}\n".format(os.urandom(128)))
                    continue

                # generate a random admin password
                if "ADMIN_DEFAULT_PASSWORD" in line and "\"pleasechangepassword\"" in line:
                    import base64
                    config.write("ADMIN_DEFAULT_PASSWORD = \"{}\"\n".format(
                        base64.encodebytes(os.urandom(20)).decode().replace("\n","")
                    ))
                    continue

                # comment out defaults
                if line[0] not in " #\r\n":
                    config.write("#")

                config.write(line)
    print("SUCCESS: config file generated at '{}'".format(cfg))

def init(filename=None, warn=True):
    """
        clear the database and create new tables
    """
    filename = filename or current_app.config["DATABASE"]
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    if os.path.exists(filename):
        if warn:
            print("WARNING: No database initialised. "
            "Another database already exists at `{}`.".format(filename))
        return

    db = get(check_exists=False, check_version=False)
    executescript(db, "sql/schema.%d.sql" % schema_version)
    print("SUCCESS: database initialised at '{}'.".format(filename))

def update():
    """
        read old format of database and store in new format
    """
    db = get(check_version=False)
    target_version = schema_version
    get_db_version = lambda: db.execute("PRAGMA user_version").fetchone()["user_version"]
    set_db_version = lambda v: db.execute("PRAGMA user_version = %d" % v)
    current_version = get_db_version()

    assert(type(current_version) == type(1))

    if current_version == target_version:
        print("Already up to date!")
        return 0

    if current_version > target_version:
        print("Your database's version number is too high!")
        print("version required:", target_version)
        print("current version of database:", current_version)
        return 1

    print("Backing up database")
    bkp = current_app.config["DATABASE"] + ".backup"
    if os.path.exists(bkp):
        raise FileExistsError("An old backup was found at `{}`. Remove it and try again.".format(bkp))
    copyfile(current_app.config["DATABASE"], bkp)

    if current_version == 0:
        print("Updating from version 0 to 1")

        # rename table `user` to `userold`
        executescript(db, "sql/update.1.sql")

        # create new tables
        executescript(db, "sql/schema.1.sql")

        # iterate over all users
        for u in db.execute("SELECT * FROM userold").fetchall():
            # copy contents into new user table
            print("Copying user", u["id"])
            db.execute(
                "INSERT INTO user (id, username, email, password, settings) VALUES (?, ?, ?, ?, ?)",
                (u["id"], u["username"], u["email"], u["password"], u["settings"])
            )

            # read events from user table
            events = u["events"].replace("\r", "").split("\n")
            print("Converting events to new format for user", u["id"])
            for e in events:
                if len(e) > 0:
                    # write events to table events
                    db.execute("INSERT INTO event (ownerid, text) VALUES (?, ?)", (u["id"], e))
        db.commit()

        # delete table `userold`
        print("Cleaning up")
        db.execute("DROP TABLE IF EXISTS userold");
        db.commit()

        current_version = get_db_version()

    if current_version != target_version:
        print("Update failed!")
        return 1

    print("Update complete!")
    return 0

def register(app):
    """
        register database functions in application
    """
    app.teardown_appcontext(close)
    app.cli.add_command(click.command("config-init")(with_appcontext(generate_config)))
    app.cli.add_command(click.command("db-init")(with_appcontext(init)))
    app.cli.add_command(click.command("db-update")(with_appcontext(update)))
    return app
