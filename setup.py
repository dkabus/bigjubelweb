from setuptools import setup

setup(
    name='bigjubelweb',
    version='0.7.7',
    description='Web interface to bigjubel',
    url='https://gitlab.com/dkabus/bigjubelweb',
    author='Desmond Kabus',
    author_email='desmond.kabus@protonmail.com',
    license='GPLv3+',
    packages=['bigjubelweb'],
    install_requires=[
        'flask',
        'werkzeug',
        'PyYAML',
        'click',
    ],
    entry_points = {
        'console_scripts': ['bigjubelweb=bigjubelweb.webcli:main'],
    },
    include_package_data=True,
    zip_safe=False,
)
