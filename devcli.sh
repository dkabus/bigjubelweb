#!/bin/sh
# Use this command to run the Flask app in this folder in development mode
FLASK_APP=bigjubelweb/main.py FLASK_ENV=development flask "$@"
