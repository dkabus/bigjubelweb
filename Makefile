all:

icon:
	convert bigjubelweb/static/logo32.png -filter point \
	-define icon:auto-resize="256,128,96,64,48,32,16" \
	bigjubelweb/static/favicon.ico

manpage-test:
	nroff -man bigjubelweb.1 | less

install:
	install -Dm644 "LICENSE" -t "$(DESTDIR)/usr/share/licenses/bigjubelweb/"
	install -Dm644 "bigjubelweb.1" -t "$(DESTDIR)/usr/share/man/man1/"
	@echo License and manual successfully installed.
	@echo To install the python package, run: python setup.py install

uninstall:
	rm -f "$(DESTDIR)/usr/share/licenses/bigjubelweb/LICENSE"
	rmdir "$(DESTDIR)/usr/share/licenses/bigjubelweb"
	rm -f "$(DESTDIR)/usr/share/man/man1/bigjubelweb.1"

.PHONY: all icon install uninstall
