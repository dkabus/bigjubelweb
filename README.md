# BigJubel Web Interface

> Calculate jubilees that you might otherwise miss,
> for instance the 7777-day jubilee of your birth!

This is a simple Flask and SQLite based web interface for [BigJubel](https://gitlab.com/dkabus/bigjubel).
It is designed to be easy to use by one or multiple users,
visually pleasing, and able to run on a local or public server.

It builds upon the command line utility [`bigjubel`](https://gitlab.com/dkabus/bigjubelcli)
and can be run as a glossy desktop app using [`bigjubeldesktop`](https://gitlab.com/dkabus/bigjubeldesktop).

The source code of `bigjubelweb` is open and can be found here:
<https://gitlab.com/dkabus/bigjubelweb>

For help and usage, check out the manuals for BigJubel here:

- [`man bigjubelweb`](https://dkabus.gitlab.io/bigjubelweb/man.html)
- [`man bigjubel`](https://dkabus.gitlab.io/bigjubel/man.html)

## Installation
On Arch Linux based distibutions, you can install the [AUR package `bigjubelweb-git`](https://aur.archlinux.org/packages/bigjubelweb-git/).

Alternatively, follow these steps:

First install the dependencies:
- BigJubel CLI `bigjubel`
- SQLite
- Python
- Flask `python-flask`
- Werkzeug `python-werkzeug`
- Click `python-click`
- YAML `python-yaml`

Create a virtual environment and install the Python package:
```sh
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -e .
```

To generate blank configuration files (in `${XDG_DATA_DIR:-$HOME/.local/share}/bigjubelweb`), run:
```sh
$ bigjubelweb config-init
$ bigjubelweb db-init
```
You might want to modify the server settings in `$HOME/.local/bin/bigjubelweb/bigjubel.sqlite`,
for instance to enable multi-user support and


When migrating from an old database, you might want to move your data to that location and run:
```sh
$ bigjubelweb db-update
```

To run a simple built-in server, run:
```sh
$ bigjubelweb run
```
and navigate to [`http://127.0.0.1:5000/`](http://127.0.0.1:5000/).

## Deployment using  `nginx` and `gunicorn`

This is an example of how to set up `bigjubelweb` using `nginx` and `gunicorn` on Arch Linux.

Create and lock user `bigjubel`:
```sh
# useradd -m bigjubel
# passwd -l bigjubel
```

Install `nginx` and `gunicorn`:
```sh
# pacman -S nginx gunicorn
```

Create a site for `nginx` in `/etc/nginx/sites-available/bigjubel.example.com.conf`:

```
server {
    listen 80;
    server_name bigjubel.example.com;

    location / {
        proxy_pass http://127.0.0.1:8000/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

Link it to `/etc/nginx/sites-enabled/bigjubel.example.com.conf`:

```sh
# ln -s /etc/nginx/sites-available/bigjubel.example.com.conf /etc/nginx/sites-enabled/bigjubel.example.com.conf
```

To create a new Systemd service unit for BigJubel using `gunicorn`, run:
```sh
# systemctl edit --force --full bigjubelweb.service
```

The content should be:
```conf
[Unit]
Description=Gunicorn instance to serve BigJubel Web
After=network.target

[Service]
User=bigjubel
Group=bigjubel
WorkingDirectory=/home/bigjubel
ExecStart=/usr/bin/gunicorn -w 4 -b 127.0.0.1:8000 'bigjubelweb:create_app()'

[Install]
WantedBy=multi-user.target
```

Enable and start everything:
```sh
# systemctl enable --now bigjubelweb.service
# systemctl enable --now nginx.service
```

## Deployment using `apache2` and `mod_wsgi`

This is an example of how to set up `bigjubelweb` using `apache2` and `wsgi` on Arch Linux.

Create and lock user `bigjubel`:
```sh
# useradd -m bigjubel
# passwd -l bigjubel
```

Install `apache2` and `mod_wsgi`:
```sh
# pacman -S apache2 mod_wsgi
```

In `/etc/httpd/conf/httpd.conf` load the module by adding this line:
```
LoadModule wsgi_module modules/mod_wsgi.so
```

Create an entry in the `apache2` configuration:
```
WSGIDaemonProcess bigjubel user=bigjubel group=bigjubel threads=5
WSGIScriptAlias /bigjubel /srv/bigjubel/bigjubel.wsgi
<Directory /srv/bigjubel>
  WSGIProcessGroup bigjubel
  WSGIApplicationGroup %{GLOBAL}
  Require all granted
</Directory>
```

Create `/srv/bigjubel/bigjubel.wsgi` with these contents:
```py
from bigjubelweb import create_app
application = create_app()
```

Make sure the user `bigjubel` can read and write to `$HOME/.local/bin/bigjubelweb/bigjubel.sqlite`.

Restart apache and navigate to <http://localhost/bigjubel>.
